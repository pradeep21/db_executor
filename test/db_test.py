from db_executor.db_executor.specific_db_exe import sql_db_exe,solr_db_exe
import traceback

try:
    with sql_db_exe.SQLDBExecutor("music_site_data") as sql_conn:
        a=sql_conn.execute_query("SELECT * from  music_song")
        for i in a:
            print("data: ",i)
except Exception as e:
    pass
    print((traceback.print_exc(), e))


try:
    1/0
    with solr_db_exe.SolrDBExecutor("films") as solr_conn:
        health=solr_conn.health_check()
        search_result=solr_conn.search("9")
        print(search_result)
        data = [{"name": ["9"], "directed_by": ["Shane Acker"],
                 "genre": ["Computer Animation", "Animation", "Apocalyptic and post-apocalyptic fiction",
                           "Science Fiction",
                           "Short Film", "Thriller", "Fantasy"], "id": "/kr/9_20051311111113",
                 "initial_release_date": ["2005-04-21T00:00:00Z"]}]
        insert_list = solr_conn.insert(data)
        print(insert_list)
        dele=solr_conn.delete("id:/jp/9_20051311111113")
        print(dele)

except Exception as e:
    pass
    print((traceback.print_exc(), e))

try:
    with solr_db_exe.SolrDBExecutor("films") as solr_conn:
        health=solr_conn.health_check()
        query_url='http://localhost:8983/solr/films/select'
        params = (

            ('q', '*:*'),
        )
        search_result=solr_conn.execute_query(query_url,**{"params":params,"method":"GET"})
        query_url = 'http://localhost:8983/solr/films/update'
        data = [{"delete": {"id":"/en/a_short_film_about_john_bolton" }}]
        search_result = solr_conn.execute_query(query_url, **{"data": data, "method": "POST"})

        print(search_result)
        params = (
            ('commitWithin', '1000'),
            ('overwrite', 'true'),
            ('wt', 'json'),
        )
        data = [{"name": ["9"], "directed_by": ["Shane Acker"],
                 "genre": ["Computer Animation", "Animation", "Apocalyptic and post-apocalyptic fiction",
                           "Science Fiction", "Short Film", "Thriller", "Fantasy"], "id": "/en1/9_200513",
                 "initial_release_date": ["2005-04-21T00:00:00Z"]}]
        data.append({"name": ["9"], "directed_by": ["Shane Acker"],
                     "genre": ["Computer Animation", "Animation", "Apocalyptic and post-apocalyptic fiction",
                               "Science Fiction", "Short Film", "Thriller", "Fantasy"], "id": "/en2/9_200512",
                     "initial_release_date": ["2005-04-21T00:00:00Z"]})

        search_result = solr_conn.execute_query(query_url, **{"data": data, "method": "POST"})

except Exception as e:
    pass
    print((traceback.print_exc(), e))