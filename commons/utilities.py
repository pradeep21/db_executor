class ListChuck:
    def __init__(self,data,chucksize):
        self.data=data
        self.chunksize=chucksize
    def __enter__(self):
        return self.get_chuck()

    def get_chuck(self):
        for i in range(0, len(self.data), self.chunksize):
            yield self.data[i:i+self.chunksize]

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.data
        del self.chunksize
