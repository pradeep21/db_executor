import os
import socket
import requests
from netifaces import interfaces, ifaddresses, AF_INET
import xmltodict
scr_dir=os.path.dirname(os.path.realpath(__file__))
from db_executor.commons.constants import ALLOWED_ENVIRONMENTS

os.environ["DBExe_ENV"]=os.getenv('DBExe_ENV', 'DBExe_LOCAL')


def get_root_dir():
    rootdir = '/'.join(os.getcwd().split('/')[0:-1])
    return rootdir

def get_credential_key():
    return  b'7RqBgLh9qATVID2nqkZAeQz33Qp694fcri6K1vJqk6U='



def get_sql_insert_prepared_statement(json_data,table_name):
    #"INSERT INTO music_song (file_type, song_title, album_id) VALUES (%s,%s,%s)"
    prepared_statement="INSERT INTO %s "%(table_name)

    all_columns="("
    values="("
    for column in json_data:
        all_columns+=str(column)+","
        values+="%s,"
    all_columns=all_columns[:-1]+")"
    values=values[:-1]+")"
    prepared_statement+=all_columns+" VALUES "+values

    return prepared_statement

def get_machine_info(public_ip=None):
    machine_info={}
    machine_info["mac_user"]=socket.gethostname()
    try:
        machine_info["local_ip"]="127.0.0.1"
        for ifaceName in interfaces():
            addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr': 'No IP addr'}])]
        machine_info["local_ip"] = addresses[0]
    except Exception as e:
        pass

    if public_ip:
        try:
            machine_info["public_ip"]=requests.get('https://api.ipify.org',timeout=3).text
        except Exception as e:
            pass

    return machine_info


def xml_to_dict(in_xml,**kwargs):
    return xmltodict.parse(in_xml)

def json_to_xml(in_json,**kwargs):
    return xmltodict.unparse(in_json)


def is_prod():
    env = os.getenv('DBExe_ENV')
    return True if env == 'DBExe_PROD' else False


def is_staging():
    env = os.getenv('DBExe_ENV')
    return True if env == 'DBExe_STAGE' else False

def deny_action():
    env = os.getenv('DBExe_ENV')
    return True if env not in ALLOWED_ENVIRONMENTS else False