from db_executor.utils.utilities import *

import json
in_xml="""<note>
<to>John</to>
<from>Mike</from>
<heading>Reminder</heading>
<body>Meet me at 7</body>
</note>"""


json_data=xml_to_dict(in_xml)
print(json.dumps(json_data))

print(json_to_xml(json_data))