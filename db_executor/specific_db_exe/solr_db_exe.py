from db_executor.db_executor.generic_db_exe import GenericDBExecutor
import simplejson as json
import time
import logging
from db_executor.logs.logger import LogDecorator,get_logger
log=get_logger(__name__)
log.setLevel(logging.DEBUG)
import pysolr
from urllib.parse import urljoin
import requests
from db_executor.commons.utilities import ListChuck
from lxml import html
class SolrDBExecutor(GenericDBExecutor):
    DB_INFO={"db":"Solr"}
    @LogDecorator(__file__)
    def __init__(self, core_name, domain="http://localhost:8983/solr/", **kwargs):
        super().__init__( core_name, domain=domain, **kwargs)
        self.port_no=kwargs.get("port",3303)
        self.user=kwargs.get("user","root")
        self.domain=domain
        self.solr_api_ep=domain
        self.core_name = core_name
        self.insert_chuck_size=kwargs.get("insert_chuck_size",10)
        self.timeout = kwargs.get("timeout", 15)

    @LogDecorator(__file__)
    def get_db_instance(self):
        if self.core_name not in self.domain:
            self.solr_api_ep=urljoin(self.domain,self.core_name)
        self.solr_admin_api=urljoin(self.solr_api_ep+"/","admin/ping")
        self.solr_contn = pysolr.Solr(self.solr_api_ep, timeout=self.timeout)
        return self.solr_contn


    def close_db(self):
        del self.solr_contn
        log.info("closed the sql-db connection")

    def health_check(self,kwargs={}):
        if not kwargs:
            kwargs=self.kwargs
        res_msg="alive"
        try:
            res = requests.get(self.solr_admin_api, timeout=self.timeout)
            res.json()
        except:
            res_msg="dead"
        return res_msg

    def update_start_query_stats(self,query_stats):
        stats={}
        stats["attempt"]=len(query_stats)+1
        stats["start_time"]=time.time()
        query_stats.append(stats)

    def update_end_query_stats(self,query_stats,kwargs):
        stats={}
        stats["end_time"]=time.time()
        if kwargs.get("json_resp") and kwargs["json_resp"].get("responseHeader",{}).get("status")==0:
            if kwargs.get("data_len"):
                stats["no_rows_aff"]=kwargs.get("data_len")
            stats["Qtime"] = kwargs["json_resp"].get("responseHeader", {}).get("QTime")
        elif kwargs.get("html_resp"):
            e = html.fromstring(kwargs.get("html_resp",'').encode())
            res_data=e.xpath('.//int[@name="status"]/text()')
            if res_data and res_data[0]=='0' and kwargs.get("data_len"):
                stats["no_rows_aff"]=kwargs.get("data_len")
            stats["Qtime"] = e.xpath('.//int[@name="QTime"]/text()')
        query_stats[-1].update(stats)


    def execute_query_with_retry(self,function,query,query_stats,kwargs):
        query_data = []
        try:
            self.update_start_query_stats(query_stats)
            if kwargs["query"]=="search":
                query_data=function(query,kwargs.get("search_params",{}))
                #query_data=json.loads(query_data)
                self.update_end_query_stats(query_stats, {"json_resp": query_data.raw_response, "data_len": kwargs.get("data_len")})
            elif kwargs["query"]=="delete":
                query_data=function(q=query)
                self.update_end_query_stats(query_stats, {"json_resp": query_data, "data_len": kwargs.get("data_len")})
        except Exception as e:
            query_stats[-1].update({"exception": repr(e)})
            raise e

        return query_data


    def search(self,text,**kwargs):
        retry = self.generic_retry()
        query_stats = []
        try:
            kwargs["query"] = "search"
            result=retry.call(self.execute_query_with_retry, self.solr_contn.search,text,query_stats,kwargs)
            return result.raw_response
        except Exception as e:
            log.error("exception in search "+repr(e))

    def delete(self,text,**kwargs):
        retry = self.generic_retry()
        query_stats = []
        try:
            kwargs["query"] = "delete"
            result = retry.call(self.execute_query_with_retry, self.solr_contn.delete, text, query_stats, kwargs)
            return result
        except Exception as e:
            log.error("exception in search " + repr(e))

    def call_api_with_retry(self, query_url, query_stats, kwargs):
        try:
            self.update_start_query_stats(query_stats)
            timeout = self.timeout
            if kwargs.get("timeout"):
                timeout = kwargs["timeout"]
            resp = requests.request(kwargs["method"], query_url, headers=kwargs["headers"], params=kwargs.get("params", ((),)), data=kwargs.get("data", '{}'), timeout=timeout)

            self.update_end_query_stats(query_stats, {"json_resp": resp.json(), "data_len": kwargs.get("data_len")})
            return resp

        except Exception as e:
            query_stats[-1].update({"exception": repr(e)})
            raise e


    def insert_batch(self,records,query_stats,**kwargs):
        try:
            self.update_start_query_stats(query_stats)
            html_resp=self.solr_contn.add(records)#commit=True
            self.update_end_query_stats(query_stats, {"html_resp":html_resp, "data_len":kwargs.get("data_len")})
            query_stats[-1].update({"no_rows_aff":len(records)})
        except Exception as e:
            query_stats[-1].update({ "exception": repr(e)})
            raise e

        return html_resp

    def __enter__(self):
        self.db_inst = self.get_db_instance()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.solr_contn.commit()
        del self.solr_contn

    def insert(self,record_list,callbacks=None,**kwargs):
        chunk_size=kwargs.get("insert_chuck_size",self.insert_chuck_size)
        retry = self.generic_retry()
        with ListChuck(record_list,chunk_size) as list_chunck:
            for chunck in list_chunck:
                query_stats = []
                retry.call(self.insert_batch, chunck, query_stats)



    def execute_query(self,query_url,callback=None,**kwargs):
        try:
            method=kwargs.get("method","GET")
            headers= {'Accept': 'application/json', 'Content-type': 'application/json'}
            if kwargs.get("headers"):
                headers=kwargs.get("headers")
            retry = self.generic_retry()
            query_stats=[]
            query_req_info={"method":method,"headers":headers,"params":kwargs.get("params",(()))}
            if kwargs.get("data"):
                if not isinstance(kwargs["data"],str):
                    query_req_info["data_len"]=len(kwargs["data"])
                    query_req_info["data"]=json.dumps(kwargs["data"])
            resp=retry.call(self.call_api_with_retry,query_url,query_stats,query_req_info)
            return resp

        except Exception as e:
            log.error("some exception in execute_query: "+repr(e))
            callback()


