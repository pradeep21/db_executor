from db_executor.db_executor.generic_db_exe import GenericDBExecutor
import simplejson as json
import pymysql
from db_executor.utils import utilities
import time
import logging
import tenacity
#from  pymysql import  cursors
from db_executor.logs import logger
from db_executor.commons.utilities import ListChuck
from db_executor.utils.utilities import is_prod,is_staging
from db_executor.logs.logger import LogDecorator,get_logger
log=get_logger(__name__)
log.setLevel(logging.DEBUG)

class SQLDBExecutor(GenericDBExecutor):
    DB_INFO={"db":"SQL"}
    @LogDecorator(__file__)
    def __init__(self, db_name, domain="localhost", **kwargs):
        super().__init__( db_name, domain="localhost", **kwargs)
        self.port_no=kwargs.get("port",3306)
        self.user=kwargs.get("user","root")
        self.db_type=kwargs.get("db_type","sql")
        self.db_name = db_name#kwargs.get("db_name", "test")
        self.domain=domain
        self.chuck_size=kwargs.get("chuck_size",10)

    def get_db_meta(self):
        base_db=super().get_db_meta()
        db_meta={"chuck_size":self.chuck_size,"port_no":self.port_no}
        db_meta["user"]=self.user
        db_meta["kwargs"]=self.kwargs
        db_meta.update(base_db)
        return db_meta

    def get_db_conn_instance(self):
        self.db_conn_inst=None
        try:
            log.debug("In get_db_instancemethod of SQLDBExecutor ")
            key=utilities.get_credential_key()
            passwd=self.get_db_credentials(key,self.domain,self.db_type,self.db_name,self.user)#,self.kwargs)
            self.db_conn_inst = pymysql.connect(host=self.domain, port=self.port_no, user=self.user, passwd=passwd, db=self.db_name)
        except Exception as e:
            log.error("exception while creating sql db instance: "+repr(e))

        return self.db_conn_inst


    def close_db(self):
        try:
            self.db_conn_inst.close()
            log.info("closed the sql-db connection")
        except Exception as e:
            log.error("exception while closing sql db instance: " + str(e))



    def start_query_stats(self, query_stats):
        stats={}
        stats["attempt"]=len(query_stats)+1
        stats["start_time"]=time.time()
        query_stats.append(stats)

    def end_query_stats(self, query_stats):
        stats={}
        stats["end_time"]=time.time()
        query_stats[-1].update(stats)



    def execute_query_with_retry(self, function, query, query_stats):
        try:
            self.start_query_stats(query_stats)
            no_rows_aff=function(query)
            self.end_query_stats(query_stats)
            query_stats[-1].update({"no_rows_aff": no_rows_aff, "lastrowid": self.cursor.lastrowid})
        except Exception as e:
            log.error("exception in execute_query_with_retry: " + str(e))
            query_stats[-1].update({"exception": repr(e)})
            raise e

        self.log_metadata(data=query_stats)


    def insert_batch(self,prepared_statement,records,query_stats):
        try:
            self.start_query_stats(query_stats)
            no_rows_aff=self.cursor.executemany(prepared_statement, records)
            self.db_conn_inst.commit()
            self.end_query_stats(query_stats)
            query_stats[-1].update({"no_rows_aff":no_rows_aff,"lastrowid":self.cursor.lastrowid})
        except Exception as e:
            log.error("exception in insert_batch: " + str(e))
            query_stats[-1].update({ "exception": repr(e)})
            raise e

        crud_meta={"crud_name":"insert"}
        crud_stats={"crud_meta":crud_meta,"query_stats":query_stats}
        self.log_metadata(data=crud_stats)

        return self.cursor.fetchall()

    def __enter__(self):
        self.db_conn_inst = self.get_db_conn_instance()
        self.cursor = self.db_conn_inst.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_db()

    def insert(self,record_list,callbacks=None,**kwargs):
        try:
            log.debug("in insert method")
            chunk_size=kwargs.get("chuck_size",self.chuck_size)
            prepared_statement=utilities.get_sql_insert_prepared_statement(record_list[0])
            retry = self.generic_retry()
            # for i in range(0,len(record_list),chunk_size):
            #     query_stats=[]#{"attempt":0,"exception_details":[]}
            #     retry.call(self.insert_batch,prepared_statement,record_list[i:i+chunk_size],query_stats)

            with ListChuck(record_list, chunk_size) as list_chunck:
                for chunck in list_chunck:
                    query_stats = []
                    retry.call(self.insert_batch, prepared_statement,chunck, query_stats)
        except Exception as e:
            log.error("exception in insert: " + str(e))


    def execute_query(self,query,**kwargs):
        retry = self.generic_retry()
        query_stats=[]
        retry.call(self.execute_query_with_retry,self.cursor.execute,query,query_stats)
        for row in  self.cursor.fetchall():
            yield row

