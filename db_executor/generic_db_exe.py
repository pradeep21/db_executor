

from db_executor.utils import  utilities
from cryptography.fernet import Fernet
import tenacity,time,datetime
from db_executor.logs import logger
from db_executor.lib.kafkalib_handler import DBExeKafkaProducer,DBExeKafkaAPIProducer
from db_executor.utils.utilities import is_prod
import os
log=logger.get_logger(__name__)
try:
    from db_executor.lib.kafkalib import kafka_producer
except Exception as e:
    log.critical("Failed to import kafkalib"+repr(e))




class GenericDBExecutor:
    DB_SUPPORTED=["sql","solr"] # needed?

    def __init__(self,db_name,domain="localhost",**kwargs):
        self.db_name=db_name
        self.domain=domain
        self.kwargs=kwargs
        self.db_instance=None
        self.source_ip=utilities.get_machine_info()
        self.kafka_topic = kwargs.get("kafka_topic", "pl_db_executor_logs")
        self.kafka=None
        try:
            if is_prod:
                self.kafka=DBExeKafkaProducer(self.kafka_topic,1)
            else:
                self.kafka = DBExeKafkaAPIProducer(self.kafka_topic,1)
        except Exception as e:
            log.error("some exception in creating kafka object {}".format(repr(e)))

    def get_db_instance(self):
        raise NotImplementedError("Subclasses should implement this!")

    def get_db_meta(self):
        db_metadata={"db_name":self.db_name,"domain":self.domain,"source_machine":self.source_ip}
        return db_metadata

    def log_metadata(self, data):
        try:
            self.kafka.push([data])
        except Exception as e:
            log.error("exception in log_metadata to kafka:{},data pushed{} ".format(repr(e),[data]))


    @staticmethod
    def get_db_credentials(key,domain,db_type,db_name,user="",**kwargs):
        file_name=domain.split("//")[-1]+db_type+db_name+user
        root_path=utilities.get_root_dir()
        #cred_key=utilities.get_credential_key()
        cipher_suite = Fernet(key)
        with open(root_path+"/bin/"+file_name) as fptr:
            for line in fptr:
                encryptedpwd = line
        uncipher_text = (cipher_suite.decrypt(encryptedpwd.encode()))
        pwd = bytes(uncipher_text).decode()  # convert to string
        return pwd



    @staticmethod
    def generic_retry(wait_time=1,no_retries=3,excep=Exception):
        return tenacity.Retrying(
            wait=tenacity.wait_fixed(wait_time),
            retry=tenacity.retry_if_exception_type(excep), stop=tenacity.stop_after_attempt(no_retries),)




    @staticmethod
    def log_info_kafka(kafka_obj,stats):
        pass

    def execute_query(self,query,callback=None,**kwargs):
        raise NotImplementedError("Subclasses should implement this!")


    def insert(self,record_list,**kwargs):
        raise NotImplementedError("Subclasses should implement this!")


