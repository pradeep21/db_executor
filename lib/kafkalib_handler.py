import json
import logging

import requests

from db_executor.commons.constants import KAFKA_API
from db_executor.lib.kafkalib import kafka_producer

logger = logging.getLogger(__name__)


class DBExeKafkaProducer:

    def __init__(self, topic):
        self.producer = None
        self.topic = topic
        #self.producer=kafka_producer.kafka()
        self.producer=kafka_producer.kafka()

    def __enter__(self):
        # producer = kafka_producer.kafka()
        # self.producer = producer
        #
        return self

    def push(self, item, log_dict={}):
        self.producer.send(self.topic, item, log_dict)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.producer.close()

    def __del__(self):
        self.producer.close()


class DBExeKafkaAPIProducer(DBExeKafkaProducer):

    def __init__(self, topic, buffer_size=10, timeout=20):
        super().__init__(topic)
        self.buffer = []
        self.buffer_size = buffer_size
        self.timeout = timeout

    def __enter__(self):
        return self

    def post(self):
        response = requests.post(KAFKA_API, data=json.dumps(self.buffer), timeout=self.timeout)
        logger.info('Pushed {} items to kafka.\n Response: {}'.format(len(self.buffer),response.json()))
        self.buffer = []

    def push(self, item, log_dict={}):
        item['kafka_topic'] = self.topic
        self.buffer.append(item)

        if len(self.buffer) >= self.buffer_size:
            self.post()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.buffer:
            self.post()


class DummyProducer(DBExeKafkaProducer):

    def __enter__(self):
        return self

    def push(self, item, log_dict={}):
        msg = '\n'
        msg += '\n'.join(['{}: {}'.format(key,value) for key, value in item.items()])
        logger.info(msg)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
